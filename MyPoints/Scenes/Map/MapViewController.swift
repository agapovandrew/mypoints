//
//  MapViewController.swift
//  MyPoints
//
//  Created by Agapov on 5/5/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import UIKit
import GoogleMaps
import RxSwift
import RxCocoa

class MapViewController: UIViewController {
    
    // MARK: - Public properties
    
    public let viewModel: MapViewModel
    
    // MARK: - Private properties
    
    private var mapView = GMSMapView()
    private var clusterManager: GMUClusterManager?
    private let disposeBag = DisposeBag()
    
    private let defaultZoom: Float = 10
    private let screenName = "Карта"
    
    // MARK: - Init
    
    init(viewModel: MapViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configureUI()
        self.setupMapView()
        self.bindViewModel()
    }
    
    override func loadView() {
        self.view = mapView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.viewModel.input.toggleUpdateLocation.onNext(())
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.viewModel.input.refreshMap.onNext(())
    }
    
    private func configureUI() {
        self.navigationItem.title = self.screenName
    }
    
    private func setupMapView() {
        self.mapView.delegate = self
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = CustomClusterRenderer(mapView: mapView, clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        
        self.clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm, renderer: renderer)
        self.clusterManager?.setDelegate(self, mapDelegate: self)
    }
    
    private func addMarkersFor(points: [MapPoint], with selectedPoint: MapPoint?) {
        
        self.clusterManager?.clearItems()
        
        points.forEach { point in
            if selectedPoint?.id == point.id {
                mapView.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: point.latitude, longitude: point.longitude) , zoom: self.defaultZoom)
            }
            let item = ClusterItem(position: point.position, name: point.pointName, id: point.id)
            self.clusterManager?.add(item)
        }
        
        self.clusterManager?.cluster()
    }
    
    
    // MARK: - ViewModel binding
    
    private func bindViewModel() {
        self.viewModel.output.currentLocation
            .filter{ [weak self] _ in
                return self?.viewModel.output.selectedPoint.value == nil
            }
            .asDriver(onErrorJustReturn: nil)
            .drive(onNext: {[weak self] location in
                guard let self = self, let location = location else {return}
                self.mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: self.defaultZoom)
            }).disposed(by: self.disposeBag)
        
        self.viewModel.output.points
            .asDriver()
            .drive(onNext: {[weak self] points in
                self?.addMarkersFor(points: points, with: nil)
            }).disposed(by: self.disposeBag)
        
        self.viewModel.output.selectedPoint
            .asDriver()
            .filter{$0 != nil}
            .drive(onNext: {[weak self] point in
                guard let self = self else {return}
                self.addMarkersFor(points: self.viewModel.output.points.value, with: point)
            }).disposed(by: self.disposeBag)
        
        self.viewModel.output.errorMessage
            .asDriver(onErrorJustReturn: "Error")
            .drive(onNext: { errorMessage in
                //TODO: Show error message
                print(errorMessage)
            }).disposed(by: self.disposeBag)
    }
    
    
}

// MARK: - ClusterRenderer Delegate methods

extension MapViewController: GMUClusterRendererDelegate {
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        if let clusterItem = marker.userData as? ClusterItem {
            marker.title = clusterItem.name
        }
    }
    
    func renderer(_ renderer: GMUClusterRenderer, didRenderMarker marker: GMSMarker) {
        if let clusterItem = marker.userData as? ClusterItem,
            clusterItem.id == self.viewModel.output.selectedPoint.value?.id {
            self.mapView.selectedMarker = marker
        }
    }
    
}

// MARK: - Google MapView Delegate methods

extension MapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        self.viewModel.input.addNewPoint.onNext(coordinate)
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.viewModel.input.refreshMap.onNext(())
    }
    
}

// MARK: - GMUClusterManager Delegate methods

extension MapViewController: GMUClusterManagerDelegate {
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        return false
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap clusterItem: GMUClusterItem) -> Bool {
        return false
    }
    
}
