//
//  ClusterItem.swift
//  MyPoints
//
//  Created by Agapov on 5/7/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import Foundation
import GoogleMaps

class ClusterItem: NSObject, GMUClusterItem  {
    
    var position: CLLocationCoordinate2D
    var name: String
    var id: String
    
    init(position: CLLocationCoordinate2D, name: String, id: String) {
        self.position = position
        self.name = name
        self.id = id
    }
    
}

