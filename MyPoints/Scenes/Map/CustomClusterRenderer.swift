//
//  CustomClusterRenderer.swift
//  MyPoints
//
//  Created by Agapov on 5/7/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import Foundation

class CustomClusterRenderer: GMUDefaultClusterRenderer {
    
    private let minClusterSize: UInt = 3
    private let maxClusterZoom: Float = 20
    
    override func shouldRender(as cluster: GMUCluster, atZoom zoom: Float) -> Bool {
        return cluster.count >= minClusterSize && zoom <= maxClusterZoom
    }
    
}
