//
//  MapViewModel.swift
//  MyPoints
//
//  Created by Agapov on 5/5/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import Foundation
import XCoordinator
import Firebase
import CodableFirebase
import CoreLocation
import GoogleMaps
import RxSwift
import RxCocoa

final class MapViewModel: NSObject, CLLocationManagerDelegate {
    
    // Default location
    private static let gbkSoftOfficeLocation = CLLocation(latitude: 50.426800, longitude: 30.503347)
    
    struct Input {
        let toggleUpdateLocation: PublishSubject<Void>
        let fetchPoints: PublishSubject<Void>
        let showSpecificPoint: PublishSubject<MapPoint?>
        let refreshMap: PublishSubject<Void>
        let addNewPoint: PublishSubject<CLLocationCoordinate2D>
    }
    
    struct Output {
        let currentLocation: BehaviorRelay<CLLocation?>
        let points: BehaviorRelay<[MapPoint]>
        let selectedPoint: BehaviorRelay<MapPoint?>
        let errorMessage: PublishSubject<String>
    }
    
    // MARK: - Public properties
    
    public let input: Input
    public let output: Output
    
    // MARK: - Private properties
    
    private let router: AnyRouter<MapRoute>
    private let locationManager = CLLocationManager()
    private let toggleUpdateLocationSubject = PublishSubject<Void>()
    private let errorMessageSubject = PublishSubject<String>()
    private let fetchPointsSubject = PublishSubject<Void>()
    private let refreshMapSubject = PublishSubject<Void>()
    private let showSpecificPoint = PublishSubject<MapPoint?>()
    private let addNewPointSubject = PublishSubject<CLLocationCoordinate2D>()
    private let selectedPoint = BehaviorRelay<MapPoint?>(value: nil)
    private let currentLocation = BehaviorRelay<CLLocation?>(value: gbkSoftOfficeLocation)
    private let points = BehaviorRelay<[MapPoint]>(value: [])
    private let ref = Database.database().reference()
    private let disposeBag = DisposeBag()
    
    // MARK: - Init
    
    init(router: AnyRouter<MapRoute>) {
        self.router = router
        self.input = Input(toggleUpdateLocation: self.toggleUpdateLocationSubject, fetchPoints: self.fetchPointsSubject, showSpecificPoint: self.showSpecificPoint, refreshMap: self.refreshMapSubject, addNewPoint: self.addNewPointSubject)
        self.output = Output(currentLocation: self.currentLocation, points: self.points, selectedPoint: self.selectedPoint, errorMessage: self.errorMessageSubject)
        
        super.init()
        
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        
        self.toggleUpdateLocationSubject
            .subscribe(onNext: {[weak self] in
                self?.locationManager.startUpdatingLocation()
            }).disposed(by: self.disposeBag)
        
        self.showSpecificPoint
            .bind(to: self.selectedPoint)
            .disposed(by: self.disposeBag)
        
        self.addNewPointSubject
            .subscribe(onNext: {[weak self] coordinate in
                self?.router.trigger(.addPointAlert(coordinatesString: String(describing: coordinate), addHandler: { pointName in
                    let point = MapPoint(id: UUID().uuidString, pointName: pointName, latitude: coordinate.latitude, longitude: coordinate.longitude)
                    MapPointsStore.shared.addPointSubject.onNext(point)
                }))
            }).disposed(by: self.disposeBag)
        
        self.refreshMapSubject
            .subscribe(onNext: {[weak self] in
                self?.selectedPoint.accept(nil)
            }).disposed(by: self.disposeBag)
        
        
        // Binding MapPointsStore
        self.fetchPointsSubject
            .bind(to: MapPointsStore.shared.fetchPointsSubject)
            .disposed(by: self.disposeBag)
        
        MapPointsStore.shared.availablePoints
            .bind(to: self.points)
            .disposed(by: self.disposeBag)
        
        MapPointsStore.shared.errorMessage
            .bind(to: self.errorMessageSubject)
            .disposed(by: self.disposeBag)
    }
    
    
    // MARK: - LocationManager Delegate methods
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationManager.stopUpdatingLocation()
        guard let location = locations.first else {return}
        self.currentLocation.accept(location)
    }
    
}
