//
//  LoginViewModel.swift
//  MyPoints
//
//  Created by Agapov on 5/5/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import Foundation
import XCoordinator
import GoogleSignIn
import Firebase
import RxSwift
import RxCocoa

final class LoginViewModel: NSObject, GIDSignInDelegate {
    
    struct Input {
        let tryToLogIn: AnyObserver<Void>
    }
    
    struct Output {
        let loginInProgress: BehaviorRelay<Bool>
        let errorMessage: PublishSubject<String>
    }
    
    // MARK: - Public properties
    public let input: Input
    public let output: Output
    
    // MARK: - Private properties
    private let router: AnyRouter<AppRoute>
    private let disposeBag = DisposeBag()
    private let loginInProgress = BehaviorRelay<Bool>(value: false)
    private let isLoggedIn = PublishSubject<Void>()
    private let tryToLogInSubject = PublishSubject<Void>()
    private let errorMessageSubject = PublishSubject<String>()
    
    // MARK: - Init
    
    init(router: AnyRouter<AppRoute>) {
        
        self.router = router
        self.input = Input(tryToLogIn: self.tryToLogInSubject.asObserver())
        self.output = Output(loginInProgress: self.loginInProgress, errorMessage: self.errorMessageSubject)
        super.init()
        
        GIDSignIn.sharedInstance().delegate = self
        
        self.tryToLogInSubject
            .filter{GIDSignIn.sharedInstance().hasAuthInKeychain()}
            .subscribe(onNext: {
                self.loginInProgress.accept(true)
                GIDSignIn.sharedInstance().signIn()
            }).disposed(by: self.disposeBag)
        
        self.isLoggedIn
            .subscribe(onNext: {[weak self] in
                self?.router.trigger(.mainMenu)
            }).disposed(by: self.disposeBag)
    }
    
    // MARK: - Google SignIn Delegate method
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            self.loginInProgress.accept(false)
            self.errorMessageSubject.onNext(error.localizedDescription)
            return
        }
        
        guard let authentication = user.authentication else {
            self.loginInProgress.accept(false)
            self.errorMessageSubject.onNext("Login failed")
            return
        }
        
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            
            if let error = error {
                self.loginInProgress.accept(false)
                self.errorMessageSubject.onNext(error.localizedDescription)
                return
            }
            
            self.loginInProgress.accept(false)
            self.isLoggedIn.onNext(())
        }
    }

    
}
