//
//  LoginViewController.swift
//  MyPoints
//
//  Created by Agapov on 5/5/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import UIKit
import GoogleSignIn
import SnapKit
import RxSwift
import RxCocoa

final class LoginViewController: UIViewController, GIDSignInUIDelegate {
    
    // MARK: - Private properties
    
    private var googleSignInButton = GIDSignInButton()
    private let screenName = "My Points"
    private let viewModel: LoginViewModel
    private let disposeBag = DisposeBag()
    
    // MARK: - Init
    
    init(viewModel: LoginViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        
        self.addSubviews()
        self.addConstraints()
        self.confugureUI()
        self.bindViewModel()
        
        self.viewModel.input.tryToLogIn.onNext(())
    }
    
    private func addSubviews() {
        self.view.addSubview(self.googleSignInButton)
    }
    
    private func addConstraints() {
        self.googleSignInButton.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
    
    private func confugureUI() {
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.topItem?.title = self.screenName
    }
    
    // MARK: - ViewModel binding
    
    private func bindViewModel() {
        self.viewModel.output.loginInProgress.bind(to: self.googleSignInButton.rx.isHidden).disposed(by: self.disposeBag)
        self.viewModel.output.errorMessage
            .asDriver(onErrorJustReturn: "Error")
            .drive(onNext: { errorMessage in
                //TODO: Show error message
                print(errorMessage)
            }).disposed(by: self.disposeBag)

    }
    
    
}
