//
//  ProfileViewModel.swift
//  MyPoints
//
//  Created by Agapov on 5/5/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import Foundation
import XCoordinator
import RxSwift
import RxCocoa
import Firebase
import GoogleSignIn

final class ProfileViewModel: NSObject {
    
    struct Input {
        let doLogOut: PublishSubject<Void>
    }
    
    // MARK: - Public properties
    
    public let input: Input

    // MARK: - Private properties
    
    private let doLogOutSubject = PublishSubject<Void>()
    private let router: AnyRouter<ProfileRoute>
    private let diposeBag = DisposeBag()
    
    // MARK: - Init
    
    init(router: AnyRouter<ProfileRoute>) {
        self.router = router
        self.input = Input(doLogOut: self.doLogOutSubject)
        
        super.init()
        
        self.doLogOutSubject
            .subscribe(onNext: {[weak self] in
                self?.doLogOut()
            }).disposed(by: self.diposeBag)
        
    }
    
    private func doLogOut() {
        GIDSignIn.sharedInstance().signOut()
        
        self.router.trigger(.logOut)
    }
    
}
