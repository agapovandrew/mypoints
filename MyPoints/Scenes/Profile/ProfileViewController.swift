//
//  ProfileViewController.swift
//  MyPoints
//
//  Created by Agapov on 5/5/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class ProfileViewController: UIViewController {
 
    // MARK: - Private properties
    
    private let logoutButton = UIButton()
    
    private let viewModel: ProfileViewModel
    private let screenName = "Профиль"
    private let disposeBag = DisposeBag()
    
    // MARK: - Init
    
    init(viewModel: ProfileViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSubviews()
        self.addConstraints()
        self.configureUI()
        self.bindViewModel()
    }
    
    private func addSubviews() {
        self.view.addSubview(self.logoutButton)
    }
    
    private func addConstraints() {
        self.logoutButton.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.height.equalTo(40)
            make.width.equalTo(120)
        }
    }
    
    private func configureUI() {
        self.view.backgroundColor = .white
        self.navigationItem.title = self.screenName
        
        self.logoutButton.setTitle("Log out", for: .normal)
        self.logoutButton.setTitleColor(.darkGray, for: .normal)
        self.logoutButton.backgroundColor = .white
        self.logoutButton.layer.borderColor = UIColor.darkGray.cgColor
        self.logoutButton.layer.borderWidth = 1
        self.logoutButton.layer.cornerRadius = 8
        self.logoutButton.layer.shadowColor = UIColor.darkGray.cgColor
        self.logoutButton.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.logoutButton.layer.shadowOpacity = 0.4
    }
    
    private func bindViewModel() {
        self.logoutButton.rx.tap
        .throttle(0.5, scheduler: MainScheduler.instance)
        .bind(to: self.viewModel.input.doLogOut)
        .disposed(by: self.disposeBag)
    }

}
