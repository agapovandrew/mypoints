//
//  PointTableViewCell.swift
//  MyPoints
//
//  Created by Agapov on 5/6/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PointTableViewCell: UITableViewCell {
    
    // MARK: - Public properties
    
    public static let reuseId = String(describing: self)
    public static let cellHeight: CGFloat = 80
    
    public let viewModel = PointTableViewCellViewModel()
    
    // MARK: - Private properties
    
    private let stackView = UIStackView()
    private let pointNameLabel = UILabel()
    private let latitudeLabel = UILabel()
    private let longitudeLabel = UILabel()
    
    private var disposeBag = DisposeBag()
    
    // MARK: - Overrides
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubviews()
        self.addConstraints()
        self.configureUI()
        self.bindViewModel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.disposeBag = DisposeBag()
        self.bindViewModel()
    }
    
    private func addSubviews() {
        self.view.addSubview(stackView)
        self.stackView.addArrangedSubview(self.pointNameLabel)
        self.stackView.addArrangedSubview(self.latitudeLabel)
        self.stackView.addArrangedSubview(self.longitudeLabel)
    }
    
    private func addConstraints() {
        self.stackView.snp.makeConstraints { make in
            make.leading.top.equalToSuperview().offset(8)
            make.trailing.bottom.equalToSuperview().inset(8)
        }
    }
    
    private func configureUI() {
        self.selectionStyle = .none
        
        self.stackView.axis = .vertical
        self.stackView.alignment = .leading
        self.stackView.distribution = .fillEqually
    }
    
    private func bindViewModel() {
        self.viewModel.output.pointName
            .bind(to: self.pointNameLabel.rx.text)
            .disposed(by: self.disposeBag)
        
        self.viewModel.output.latitude
            .bind(to: self.latitudeLabel.rx.text)
            .disposed(by: self.disposeBag)
        
        self.viewModel.output.longitude
            .bind(to: self.longitudeLabel.rx.text)
            .disposed(by: self.disposeBag)
    }
    
}
