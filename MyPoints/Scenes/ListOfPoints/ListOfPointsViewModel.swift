//
//  ListOfPointsViewModel.swift
//  MyPoints
//
//  Created by Agapov on 5/5/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import Foundation
import XCoordinator
import RxSwift
import RxCocoa

final class ListOfPointsViewModel: NSObject {
    
    struct Input {
        let fetchPoints: PublishSubject<Void>
        let deletePoint: PublishSubject<Int>
        let pointSelected: PublishSubject<IndexPath>
    }
    
    struct Output {
        let points: BehaviorRelay<[MapPoint]>
        let errorMessage: PublishSubject<String>
    }
    
    // MARK: - Public properties
    
    public let input: Input
    public let output: Output
    
    // MARK: - Private properties
    private let router: AnyRouter<ListOfPointsRoute>
    private let points = BehaviorRelay<[MapPoint]>(value: [])
    private let fetchPointsSubject = PublishSubject<Void>()
    private let deletePointSubject = PublishSubject<Int>()
    private let pointSelectedSubject = PublishSubject<IndexPath>()
    private let errorMessageSubject = PublishSubject<String>()
    private let disposeBag = DisposeBag()
    
    // MARK: - Init
    init(router: AnyRouter<ListOfPointsRoute>) {
        self.input = Input(fetchPoints: fetchPointsSubject, deletePoint: deletePointSubject, pointSelected: pointSelectedSubject)
        self.output = Output(points: points, errorMessage: errorMessageSubject)
        self.router = router
        
        super.init()
        
        self.fetchPointsSubject
            .bind(to: MapPointsStore.shared.fetchPointsSubject)
            .disposed(by: self.disposeBag)
        
        self.deletePointSubject
            .bind(to: MapPointsStore.shared.deletePointAtIndexSubject)
            .disposed(by: self.disposeBag)
        
        self.pointSelectedSubject
            .subscribe(onNext:{[weak self] indexPath in
                guard let point = self?.points.value[indexPath.row] else {return}
                self?.router.trigger(.mapWithSelected(point: point))
            }).disposed(by: self.disposeBag)
        
        MapPointsStore.shared.availablePoints
            .bind(to: self.points)
            .disposed(by: self.disposeBag)
        
        MapPointsStore.shared.errorMessage
            .bind(to: self.errorMessageSubject)
            .disposed(by: self.disposeBag)
    }
    
}
