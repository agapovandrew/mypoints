//
//  ListOfPointsViewController.swift
//  MyPoints
//
//  Created by Agapov on 5/5/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class ListOfPointsViewController: UIViewController, UIGestureRecognizerDelegate {
    
    // MARK: - Private properties
    
    private let tableView = UITableView()
    private let noResultsLabel = UILabel()
    
    private let viewModel: ListOfPointsViewModel
    private let screenName = "Список"
    private let disposeBag = DisposeBag()
    
    // MARK: - Init
    
    init(viewModel: ListOfPointsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSubviews()
        self.addConstraints()
        self.configureUI()
        self.bindViewModel()
        self.setupLongPressGesture()
        
        self.viewModel.input.fetchPoints.onNext(())
    }
    
    private func addSubviews() {
        self.view.addSubview(self.tableView)
        self.view.addSubview(self.noResultsLabel)
    }
    
    private func addConstraints() {
        self.tableView.snp.makeConstraints { make in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
            make.leading.trailing.equalToSuperview()
        }
        self.noResultsLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
            make.height.equalTo(60)
        }
    }
    
    private func configureUI() {
        self.view.backgroundColor = .white
        self.navigationItem.title = self.screenName
        
        self.tableView.register(PointTableViewCell.self, forCellReuseIdentifier: PointTableViewCell.reuseId)
        self.tableView.rowHeight = PointTableViewCell.cellHeight
        self.tableView.tableFooterView = UIView()
        
        self.noResultsLabel.text = "Пока нет ни одной точки"
        self.noResultsLabel.textAlignment = .center
        self.noResultsLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        self.noResultsLabel.textColor = .lightGray
    }
    
    private func bindViewModel() {
        self.tableView.rx.itemSelected
            .bind(to: self.viewModel.input.pointSelected)
            .disposed(by: self.disposeBag)
        
        self.viewModel.output.points.bind(to: self.tableView.rx.items(cellIdentifier: PointTableViewCell.reuseId, cellType: PointTableViewCell.self)) { row, point, cell in
            cell.viewModel.fillWith(point: point)
            }.disposed(by: self.disposeBag)
        
        self.viewModel.output.points
            .asDriver()
            .map{!$0.isEmpty}
            .drive(self.noResultsLabel.rx.isHidden)
            .disposed(by: self.disposeBag)
        
        self.viewModel.output.errorMessage
            .asDriver(onErrorJustReturn: "Error")
            .drive(onNext: { errorMessage in
                //TODO: Show error message
                print(errorMessage)
            }).disposed(by: self.disposeBag)
    }
    
    // MARK: - Gesture Recognizer
    
    private func setupLongPressGesture() {
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressGesture.minimumPressDuration = 1.0
        longPressGesture.delegate = self
        self.tableView.addGestureRecognizer(longPressGesture)
    }
    
    @objc func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer){
        if gestureRecognizer.state == .began {
            let touchPoint = gestureRecognizer.location(in: self.tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPoint) {
                self.viewModel.input.deletePoint.onNext(indexPath.row)
            }
        }
    }
    
    
}
