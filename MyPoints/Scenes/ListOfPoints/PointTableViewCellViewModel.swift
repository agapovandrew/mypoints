//
//  PointTableViewCellViewModel.swift
//  MyPoints
//
//  Created by Agapov on 5/6/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class PointTableViewCellViewModel {
    
    struct Output {
        let pointName: BehaviorRelay<String?>
        let latitude: BehaviorRelay<String>
        let longitude: BehaviorRelay<String>
    }
    
    public let output: Output
    
    private let pointName = BehaviorRelay<String?>(value: nil)
    private let latitude = BehaviorRelay<String>(value: "")
    private let longitude = BehaviorRelay<String>(value: "")
    
    init() {
        self.output = Output(pointName: self.pointName, latitude: self.latitude, longitude: self.longitude)
    }
    
    public func fillWith(point: MapPoint) {
        self.pointName.accept(point.pointName == "" ? "(No Name)" : point.pointName)
        self.latitude.accept("Latitude: \(point.latitude)")
        self.longitude.accept("Longitude: \(point.longitude)")
    }
    
}
