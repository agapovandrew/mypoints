//
//  MapPoint.swift
//  MyPoints
//
//  Created by Agapov on 5/5/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import Foundation

struct MapPoint: Codable {
    
    var id: String
    var pointName: String
    var latitude: Double
    var longitude: Double
    var position: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}
