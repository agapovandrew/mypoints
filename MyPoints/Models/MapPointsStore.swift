//
//  MapPointsStore.swift
//  MyPoints
//
//  Created by Agapov on 5/6/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Firebase
import CodableFirebase

class MapPointsStore {
    
    // MARK: - Public properties
    
    public static let shared = MapPointsStore()
    public let fetchPointsSubject = PublishSubject<Void>()
    public let addPointSubject = PublishSubject<MapPoint>()
    public let deletePointAtIndexSubject = PublishSubject<Int>()
    public let availablePoints = BehaviorRelay<[MapPoint]>(value: [])
    public let errorMessage = PublishSubject<String>()
    
    // MARK: - Private properties
    
    private let ref = Database.database().reference()
    private let disposeBag = DisposeBag()
    
    
    // MARK: - Init
    
    private init(){
        self.fetchPointsSubject
            .subscribe(onNext: {[weak self] in
                self?.fetchPoints()
            }).disposed(by: self.disposeBag)
        
        self.addPointSubject
            .subscribe(onNext: {[weak self] point in
                self?.add(point: point)
            }).disposed(by: self.disposeBag)
        
        self.deletePointAtIndexSubject
            .subscribe(onNext: {[weak self] index in
                guard let self = self else {return}
                self.delete(point: self.availablePoints.value[index], at: index)
            }).disposed(by: self.disposeBag)

    }
    
    private func fetchPoints() {
        
        if let userID = Auth.auth().currentUser?.uid {
            
            ref.child("users/\(userID)/points").observeSingleEvent(of: .value) { snapshot in
                
                guard let snapshotDict = snapshot.value as? NSDictionary else {return}
                
                var points: [MapPoint] = []
                
                do {
                    try snapshotDict.forEach { pointDict in
                        let point = try FirebaseDecoder().decode(MapPoint.self, from: pointDict.value)
                        points.append(point)
                    }
                    points.sort{$0.pointName < $1.pointName}
                    self.availablePoints.accept(points)
                } catch {
                    self.errorMessage.onNext("Failed to fetch points")
                }
            }
            
        } else {
            errorMessage.onNext("Can't get current user id")
        }
        
    }
    
    private func add(point: MapPoint) {
        let sortedPoints = (availablePoints.value + [point]).sorted{$0.pointName < $1.pointName}
        self.availablePoints.accept(sortedPoints)
        do {
            let dictValue = try point.asDictionary()
            if let userID = Auth.auth().currentUser?.uid {
                ref.child("users/\(userID)/points/\(point.id)").setValue(dictValue)
            } else {
                errorMessage.onNext("Can't get current user id")
            }
        } catch {
            self.errorMessage.onNext("Failed to add new point")
        }
    }
    
    private func delete(point: MapPoint, at index: Int) {
        var points = self.availablePoints.value
        points.remove(at: index)
        self.availablePoints.accept(points)
        
        if let userID = Auth.auth().currentUser?.uid {
             ref.child("users/\(userID)/points/\(point.id)").removeValue()
        } else {
            errorMessage.onNext("Can't get current user id")
        }
        
    }
    
}
