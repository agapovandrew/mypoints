//
//  MainTabBarCoordinator.swift
//  MyPoints
//
//  Created by Agapov on 5/5/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import Foundation
import XCoordinator

enum MainRoute: Route {
    case listOfPoints
    case map
    case profile
    case logOut
}

class MainTabBarCoordinator: TabBarCoordinator<MainRoute> {
    
    // MARK: - Public properties
    
    public var superCoordinator: AppCoordinator?
    
    // MARK: - Private properties
    
    private let listOfPointsRouter: AnyRouter<ListOfPointsRoute>
    private let mapRouter: AnyRouter<MapRoute>
    private let profileRouter: AnyRouter<ProfileRoute>
    
    // MARK: - Init
    
    convenience init(appCoordinator: AppCoordinator) {
        
        let listOfPointsCoordinator = ListOfPointsCoordinator()
        listOfPointsCoordinator.rootViewController.tabBarItem = UITabBarItem(title: "Список", image: UIImage(named: "listIcon"), tag: 0)
        
        let mapCoordinator = MapCoordinator()
        mapCoordinator.rootViewController.tabBarItem = UITabBarItem(title: "Карта", image: UIImage(named: "mapIcon"), tag: 1)
        
        let profileCoordinator = ProfileCoordinator()
        profileCoordinator.rootViewController.tabBarItem = UITabBarItem(title: "Профиль", image: UIImage(named: "profileIcon"), tag: 2)
        
        self.init(listOfPointsRouter: listOfPointsCoordinator.anyRouter,
                  mapRouter: mapCoordinator.anyRouter,
                  profileRouter: profileCoordinator.anyRouter)
        
        self.superCoordinator = appCoordinator
        profileCoordinator.superCoordinator = self
        listOfPointsCoordinator.superCoordinator = self
        listOfPointsCoordinator.mapCoordinator = mapCoordinator
    }
    
    init(listOfPointsRouter: AnyRouter<ListOfPointsRoute>,
         mapRouter: AnyRouter<MapRoute>,
         profileRouter: AnyRouter<ProfileRoute>) {
        
        self.listOfPointsRouter = listOfPointsRouter
        self.mapRouter = mapRouter
        self.profileRouter = profileRouter
        
        super.init(tabs: [listOfPointsRouter, mapRouter, profileRouter], select: listOfPointsRouter)
    }
    
    // MARK: - Overrides
    
    override func prepareTransition(for route: MainRoute) -> TabBarTransition {
        switch route {
        case .listOfPoints:
            return .select(listOfPointsRouter)
        case .map:
            return .select(mapRouter)
        case .profile:
            return .select(profileRouter)
        case .logOut:
            self.superCoordinator?.trigger(.logOut)
            return .none()
        }
    }
    
}
