//
//  AppCoordonator.swift
//  MyPoints
//
//  Created by Agapov on 5/5/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import Foundation
import XCoordinator

enum AppRoute: Route {
    case login
    case mainMenu
    case logOut
}

class AppCoordinator: NavigationCoordinator<AppRoute> {
    
    // MARK: - Init
    
    init() {
        super.init(initialRoute: .login)
    }
    
    // MARK: - Overrides
    
    override func prepareTransition(for route: AppRoute) -> NavigationTransition {
        switch route {
        case .login:
            let viewModel = LoginViewModel(router: anyRouter)
            let viewController = LoginViewController(viewModel: viewModel)
            return .push(viewController)
            
        case .mainMenu:
            return .present(MainTabBarCoordinator(appCoordinator: self))
            
        case .logOut:
            return .dismissToRoot()
        }
        
    }
    
}
