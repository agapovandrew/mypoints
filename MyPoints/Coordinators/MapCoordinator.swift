//
//  MapCoordinator.swift
//  MyPoints
//
//  Created by Agapov on 5/5/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import Foundation
import XCoordinator

enum MapRoute: Route {
    case map
    case specific(point: MapPoint)
    case addPointAlert(coordinatesString: String, addHandler: ((String) -> ()) )
}

class MapCoordinator: NavigationCoordinator<MapRoute> {
    
    // MARK: - Init
    
    init() {
        super.init(initialRoute: .map)
    }
    
    // MARK: - Overrides
    
    override func prepareTransition(for route: MapRoute) -> NavigationTransition {
        switch route {
        case .map:
            let mapViewModel = MapViewModel(router: anyRouter)
            let mapViewController = MapViewController(viewModel: mapViewModel)
            return .push(mapViewController)
            
        case let .specific(point):
            ((self.viewController as! UINavigationController).viewControllers.first as! MapViewController) .viewModel.input.showSpecificPoint.onNext(point)
            return .none()
            
        case let .addPointAlert(coordinatesString, addHandler):
            let alert = UIAlertController(title: "Добавление точки", message: coordinatesString, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Отмена", style: .destructive, handler: nil))
            alert.addAction(UIAlertAction(title: "Добавить", style: .default, handler: { _ in
                guard let pointName = alert.textFields?.first?.text else {return}
                addHandler(pointName)
            }))
            alert.addTextField { textField in
                textField.placeholder = "Название точки"
            }
            return .present(alert)
        }
    }
}
