//
//  ProfileCoordinator.swift
//  MyPoints
//
//  Created by Agapov on 5/5/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import Foundation
import XCoordinator

enum ProfileRoute: Route {
    case userProfile
    case logOut
}

class ProfileCoordinator: NavigationCoordinator<ProfileRoute> {
    
    // MARK: - Public properties
    
    public var superCoordinator: MainTabBarCoordinator?
    
    // MARK: - Init
    
    init() {
        super.init(initialRoute: .userProfile)
    }
    
    // MARK: - Overrides
    
    override func prepareTransition(for route: ProfileRoute) -> NavigationTransition {
        switch route {
        case .userProfile:
            let profileViewModel = ProfileViewModel(router: anyRouter)
            let profileViewController = ProfileViewController(viewModel: profileViewModel)
            return .push(profileViewController)
            
        case .logOut:
            superCoordinator?.trigger(.logOut)
            return .none()
        }
    }
}
