//
//  ListOfPointsCoordinator.swift
//  MyPoints
//
//  Created by Agapov on 5/5/19.
//  Copyright © 2019 Agapov. All rights reserved.
//

import Foundation
import XCoordinator

enum ListOfPointsRoute: Route {
    case listOfPoints
    case mapWithSelected(point: MapPoint)
}

class ListOfPointsCoordinator: NavigationCoordinator<ListOfPointsRoute> {
    
    // MARK: - Public properties
    
    public var superCoordinator: MainTabBarCoordinator?
    public var mapCoordinator: MapCoordinator?
    
    // MARK: - Init
    
    init() {
        super.init(initialRoute: .listOfPoints)
    }
    
    // MARK: - Overrides
    
    override func prepareTransition(for route: ListOfPointsRoute) -> NavigationTransition {
        switch route {
        case .listOfPoints:
            let listOfPointsViewModel = ListOfPointsViewModel(router: anyRouter)
            let listOfPointsViewController = ListOfPointsViewController(viewModel: listOfPointsViewModel)
            return .push(listOfPointsViewController)
        case let .mapWithSelected(point):
            mapCoordinator?.trigger(.specific(point: point))
            superCoordinator?.trigger(.map)
            return .none()
        }
    }
}
